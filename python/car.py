
class Car(object):

    def __init__(self, name, color):
        self.name = name
        self.color= color
        self.throttle = 0.6

        self.current_lane = 0
        self.piece_id = 0;
        self.offset = 0
        self.velocity = 0.0
        self.acceleration = 0.0

    def update(self, data, distance):
        self.angle = data["angle"]
        self.piece_id = data["piecePosition"]["pieceIndex"]
        self.offset = data["piecePosition"]["inPieceDistance"]

        self.current_lane = data["piecePosition"]["lane"]["endLaneIndex"]


        self.acceleration = distance - self.velocity
        self.velocity = distance

        if self.velocity > 7.1:
            self.throttle = 0.0
        else:
            self.throttle = 1.0

    def __str__(self):
        return "angle:"+str(self.angle)+"\t vel:"+str(self.velocity)+"\t acc:"+str(self.acceleration)

    def throttle(self):
        return self.throttle
