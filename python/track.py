from piece import Piece
from car import Car

class Track(object):
	def __init__(self, data):
		track_info = data["race"]["track"]
		self.id = track_info["id"]
		self.name = track_info["name"]
		self.lanes = [lane["distanceFromCenter"] for lane in track_info["lanes"]]
		self.pieces = [Piece(raw_piece, self.lanes) for raw_piece in track_info["pieces"]]
		self.initial_position = (track_info["startingPoint"]["position"]["x"], track_info["startingPoint"]["position"]["y"])
		self.current_angle = float(track_info["startingPoint"]["angle"])
		self.cars = {(car_info["id"]["name"], car_info["id"]["color"]): Car(car_info["id"]["name"], car_info["id"]["color"]) for car_info in data["race"]["cars"]}
		self.laps = data["race"]["raceSession"]["laps"]
		self.max_lap_time = data["race"]["raceSession"]["maxLapTimeMs"]
		self.quick_race = data["race"]["raceSession"]["quickRace"]
		self.switch = False
		self.new_lane = ""

	def update(self, data, my_car):
		for car in data:
			if car["id"]["name"] == my_car.name:
				piece_id = car["piecePosition"]["pieceIndex"]
				piece_offset = car["piecePosition"]["inPieceDistance"]

				if my_car.piece_id != piece_id:
					print "-"
					delta_distance = piece_offset + (self.pieces[my_car.piece_id].length_in_lane[my_car.current_lane] - my_car.offset)
				else:
					delta_distance = piece_offset - my_car.offset

				my_car.update(car, delta_distance)
				if self.pieces[piece_id].type != "bend":
					next_bend = self.get_next_bend(piece_id)
					self.switch = True
					if next_bend.left_turn:
						self.new_lane = "Left"
					else:
						self.new_lane = "Right"
				break

	def get_next_bend(self, piece_id):
		for piece in self.pieces[piece_id:]:
			if piece.type == "bend":
				return piece
		for piece in self.pieces[:piece_id]:
			if piece.type == "bend":
				return piece

	def __str__(self):
		return "Nome: "+self.name
