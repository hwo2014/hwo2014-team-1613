from math import pi

class Piece(object):
	def __init__(self, raw_piece, lanes_info):
		self.type = "line" if "length" in raw_piece else "bend"
		self.switch = "switch" in raw_piece
		if self.type == "bend":
			self.angle = float(raw_piece["angle"])
			self.radius = float(raw_piece["radius"])
			self.left_turn = self.angle < 0
			self.right_turn = not self.left_turn
			self.best_entry_lane = None
			self.entry_velocity = None
			self.length_in_lane = []
			for lane, i in zip(lanes_info, range(len(lanes_info))):
				radius = float(raw_piece["radius"])
				radius += lane if self.left_turn else -lane
				self.length_in_lane.append(self.get_bend_length(raw_piece["angle"], radius))
		else:
			self.length_in_lane = [raw_piece["length"] for lane in lanes_info]

	def get_bend_length(self, angle, radius):
		return abs(float(angle))*pi*float(radius)/180
